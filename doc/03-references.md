## Travo Web
- [Bootstrap 5](https://v5.getbootstrap.com/)
- [Handlebars](https://handlebarsjs.com/)
- [Gitlab API](https://docs.gitlab.com/ee/api/)
- [QUnit](https://qunitjs.com)
--- 


## Travo CLI

- [JQ](https://stedolan.github.io/jq/)
- [Git](https://git-scm.com/)
- [Gitlab API](https://docs.gitlab.com/ee/api/)
- [Bats](https://github.com/bats-core/bats-core)
