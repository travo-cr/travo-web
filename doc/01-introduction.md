# Travo

Travo est un outil permettant de faciliter la création, et la remise des Travaux Pratiques des étudiants sur Gitlab.



Travo est disponible sous deux formes :

- [Travo CLI](https://gitlab.info.uqam.ca/info/travo)
- [Travo Web](https://gitlab.info.uqam.ca/info/travo-web)


Les tests de Travo CLI sont intégrés dans sa pipeline (BATS), et en ligne de commande en local (Shellcheck).

Les tests de Travo Web sont disponibles à cette adresse : https://info.pages.info.uqam.ca/travo-web/tests/test_results.html


La version CLI peut être utilisée par les professeurs et les étudiants, et la version Web se concentre sur les actions des étudiants (acceptation d'un TP, passage de ce dernier en privé, ajout du professeur, mise en ligne ou mise à jour d'un fichier).

___
## Changement de configuration :
La configuration de l'environnement se fait dans /public/js/travo_scripts.js.

Les quatre variables a éditer sont :
- url_oauth,
- client_id,
- url_api,
- redirect_url

Pour les 3 premières variables, il faut créer une [application Gitlab](https://docs.gitlab.com/ee/integration/oauth_provider.html).
La 4ème variable est simplement l'URL où Travo Web est accessible.

#### Personnalisation des Templates Handlebars :

Travo Web se repose sur Handlebars pour gérer ses templates.

Ils sont tous dans le dossier /public/templates.

Si un nouveau template est créé, ou simplement mis à jour, il faudra le compiler (en ayant [installé handlebars](https://handlebarsjs.com/installation/) auparavant) avec la commande suivante :
```bash
handlebars NOMDEVOTRETEMPLATE.handlebars -f NOMDEVOTRETEMPLATE.precompiled.js
```

#### Mise à jour ou ajout de tests :

Les tests utilisent QUnit, et se trouvent dans le dossier /public/tests

Une fois Travo Web déployé, les tests sont accessibles via navigateur à l'URL https://VOTREURLTRAVO/tests/test_results.html

---

## Sommaire de la documentation :

- [Utilisation](https://gitlab.info.uqam.ca/info/travo-web/-/blob/master/doc/02-usage.md)
- [Références](https://gitlab.info.uqam.ca/info/travo-web/-/blob/master/doc/03-references.md)
