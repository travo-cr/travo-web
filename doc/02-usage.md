## Utilisation

### Parcours type d'un devoir

Parcours détaillé de l'envoi d'un devoir par un professeur jusqu'à la fin d'un TP pour un étudiant

---
   - **Professeur** : Envoi d'un TP
``` html
https://info.pages.info.uqam.ca/travo-web/index.html?project=IDTP
```
Le professeur doit envoyer l'URL ci-dessus, en remplacant uniquement IDTP par l'identifiant du TP qu'il souhaite partager avec ses étudiants.

A noter que le TP partagé doit soit :
- Être public,
- Avoir les étudiants comme membres s'il est privé.

---

- **Élève** : Clique sur le lien du professeur

Après avoir cliqué sur le lien envoyé par le professeur, l'étudiant arrive sur une page vide, avec uniquement un bouton pour se connecter avec son compte Gitlab de l'UQAM.

---

- **Élève** : Est connecté, et accède à Travo sans paramètres dans l'URL, mais avec un historique de projets

Si l'étudiant accède à https://VOTREURLTRAVO/ sans argument 'project', alors il aura soit un message d'erreur affiché lui précisant que son professeur lui a envoyé une URL sans argument (s'il ne possède aucun projet enregistré dans son local storage), ou alors il aura accès à un historique de ses projets présents dans le localStorage de son navigateur.


---
- **Élève** : Se connecte avec son compte Gitlab

Après s'être connecté avec son compte Gitlab, l'élève est redirigé vers l'interface Travo Web, avec désormais un header personnalisé avec son image de profil Gitlab, et son nom.

A partir d'ici, deux possibilitées :
- Soit l'utilisateur a déjà forké le projet du professeur, et Travo Web détecte cela automatiquement et le redirige vers l'affichage de son projet,
- Soit l'utilisateur n'a pas encore forké le projet du professeur, et Travo Web lui proposera d'accepter le projet (le forker).

Sur la page d'affichage du projet du professeur, plusieurs informations sont affichées :
- Le nom du projet, sa date de création, et un lien vers le projet
- Si le dépôt Gitlab possède une description, elle est affichée
- SSH du dépôt
- Par ordre d'importance :
    - Si un fichier "sujet.md" existe, il est affiché,
    - Si le fichier "sujet.md" n'existe pas, le fichier "README.md" est affiché,
    - Si aucun des deux n'existe, l'encart n'est pas affiché.
    
---

- **Elève** : Accède a son projet forké

L'interface de visionnage du dépôt de l'étudiant est globalement similaire à celle du dépôt du professeur.

Plusieurs informations sont affichées :
- Le nom de son projet, sa date de création, et un lien vers le projet
- Si son dépôt Gitlab possède une description, elle est affichée
- SSH de son dépôt
- L'état de son dépôt :
    - Si le dépôt n'est pas privé, un bouton est affiché pour proposer à l'utilisateur de le faire
    - Si le professeur n'est pas ajouté, un bouton est affiché pour proposer à l'utilisateur de le faire
    - Si le dépôt est privé, et que le professeur est ajouté, deux vignettes affichent que toutes les démarches ont bien été faites
- Par ordre d'importance :
    - Si un fichier "sujet.md" existe, il est affiché,
    - Si le fichier "sujet.md" n'existe pas, le fichier "README.md" est affiché,
    - Si aucun des deux n'existe, l'encart n'est pas affiché.
- Si son dépôt possède une pipeline, son état sera affiché
- Si un fichier "rapport.md" existe, il est affiché
- Un champ de mise en ligne de fichier (texte, code, etc... mais pas de fichier compressé, binaire). Gitlab va uniquement mettre en ligne le contenu du fichier. Le fichier sera mis en ligne à la racine du dépôt Gitlab de l'étudiant


