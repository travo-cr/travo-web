Env DEV

Lien vers le projet : https://info.pages.info.uqam.ca/travo-web

Test de projet : https://info.pages.info.uqam.ca/travo-web/?project=312

---

Env PROD

Lien vers le projet : https://info.pages.info.uqam.ca/travo-web

Test de projet : https://info.pages.info.uqam.ca/travo-web/?project=859

---

Lien vers les tests : https://info.pages.info.uqam.ca/travo-web/tests/test_results.html


--- 

Configuration de l'environnement dans /public/js/travo_scripts.js.

Les quatre variables a éditer sont :
- url_oauth,
- client_id,
- url_api,
- redirect_url

Pour les 3 premières variables, il faut créer une [application Gitlab](https://docs.gitlab.com/ee/integration/oauth_provider.html).
La 4ème variable est simplement l'URL où Travo Web est accessible.
