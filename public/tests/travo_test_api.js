oldToken = window.localStorage.getItem('access_token');
window.localStorage.setItem('access_token', '15c3bc977a021abd8ee270f8091aa2dd37e5c7a379ff3547a2c55284811f6965');

function isArray(what) {
    return Object.prototype.toString.call(what) === '[object Array]';
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


QUnit.module('Check if file exist in Gitlab Repo', function () {
    QUnit.test('should return true if file exist', function (assert) {
        let done = assert.async();
        setTimeout(function () {
            assert.ok(fileExist(334, 'rapport.md'));
            done();
        }, 2000);
    });
    QUnit.test('should return false if file doesn\'t exist', function (assert) {
        let done = assert.async();
        setTimeout(function () {
            assert.notEqual(fileExist(334, 'randomname.md'), true);
            done();
        }, 2000);
    })
});

QUnit.module('Check if users of a Gitlab repo are returned', function () {
    QUnit.test('should return JSON if repo exist', function (assert) {
        let done = assert.async();
        setTimeout(function () {
            assert.ok(getMembersInfos(334));
            done();
        }, 2000);
    });
    QUnit.test('should return false if repo doesnt exist', function (assert) {
        let done = assert.async();
        setTimeout(function () {
            assert.notOk(isArray(getMembersInfos(22222222222222222)));
            done();
        }, 2000);
    })
});

QUnit.module('Check if the content of a file in a Gitlab repo can be fetched', function () {
    QUnit.test('should return the content of "rapport.md" of project 334', async function (assert) {
        let done = assert.async();
        await getFileContent(334, 'rapport.md');
        setTimeout(function () {
            assert.equal($id('readme_content').innerText, 'test');
            done();
        }, 5000);
    });
})
;

QUnit.module('Check if sha of the first commit of a repo is correctly fetched', function () {
    QUnit.test('should return the correct sha of the first commit of project 334', async function (assert) {
        let done = assert.async();
        let sha = await getShaParentProject(334);
        setTimeout(function () {
            assert.equal(sha, '8482915f464307afd2d18f6e84d7d1a6cfb0c436');
            done();
        }, 2000);
    });
});

QUnit.module('Check if the creation date of a repo is correctly fetched', function () {
    QUnit.test('should return the correct creation date of project 334', async function (assert) {
        let done = assert.async();
        let creationDate = await getCreationDate(334);
        setTimeout(function () {
            assert.equal(creationDate.created_at, "2020-08-04T15:41:25.684Z");
            done();
        }, 2000);
    });
});

window.localStorage.setItem('access_token', oldToken);
