QUnit.module('Get query params', function () {
    QUnit.test('should get param after question mark', function (assert) {
        assert.equal(getQueryParams("https://info.pages.info.uqam.ca/travo-web/index.html?project=329&access_token=123456abc#error=erreur", "project"), "329")
    });
    QUnit.test('should get param after ampersand', function (assert) {
        assert.equal(getQueryParams("https://info.pages.info.uqam.ca/travo-web/index.html?project_id=329&access_token=123456abc#error=erreur", "access_token"), "123456abc")
    });
    QUnit.test('should get params after hashtag', function (assert) {
        assert.equal(getQueryParams("https://info.pages.info.uqam.ca/travo-web/index.html?project_id=329&access_token=123456abc#error=erreur", "error"), "erreur")
    });
});

QUnit.module('Get element by id', function () {
    QUnit.test('should get element by its id', function (assert) {
        assert.strictEqual($id("testGetById").id, 'testGetById')
    });
});

QUnit.module('Decode B64 string', function () {
    QUnit.test('should decode a B64 string', function (assert) {
        assert.strictEqual(b64DecodeUnicode('SGVsbG8gd29ybGQ='), 'Hello world')
    });
});

