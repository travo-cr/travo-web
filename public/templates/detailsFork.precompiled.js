(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['detailsFork'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <p>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":12,"column":15},"end":{"line":12,"column":30}}}) : helper)))
    + "</p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "            <p>Aucune description trouvée.</p>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <button type=\"button\" onclick=\"makePrivate("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":20,"column":55},"end":{"line":20,"column":61}}}) : helper)))
    + ")\" class=\"btn btn-info\">Passer mon dépôt en privé</button>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"profAdded") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":21,"column":8},"end":{"line":52,"column":8}}})) != null ? stack1 : "");
},"8":function(container,depth0,helpers,partials,data) {
    return "            <div>\n                <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-check2 text-success\" fill=\"currentColor\"\n                     xmlns=\"http://www.w3.org/2000/svg\">\n                    <path fill-rule=\"evenodd\"\n                          d=\"M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z\"/>\n                </svg>\n                <p class=\"d-inline\">Dépôt privé</p>\n            </div>\n\n            <div>\n                <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-check2 text-success\" fill=\"currentColor\"\n                     xmlns=\"http://www.w3.org/2000/svg\">\n                    <path fill-rule=\"evenodd\"\n                          d=\"M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z\"/>\n                </svg>\n                <p class=\"d-inline\">Professeur ajouté</p>\n            </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <div>\n                <svg width=\"1em\" height=\"1em\" viewBox=\"0 0 16 16\" class=\"bi bi-check2 text-success\" fill=\"currentColor\"\n                     xmlns=\"http://www.w3.org/2000/svg\">\n                    <path fill-rule=\"evenodd\"\n                          d=\"M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z\"/>\n                </svg>\n                <p class=\"d-inline\">Dépôt privé</p>\n            </div>\n\n            <button type=\"button\" onclick=\"addProfessor("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":49,"column":56},"end":{"line":49,"column":62}}}) : helper)))
    + ")\" class=\"btn btn-info\">Ajouter le professeur à mon\n                dépôt\n            </button>\n        ";
},"12":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <p>Heure de création: "
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"cicd") : depth0)) != null ? lookupProperty(stack1,"created_at") : stack1), depth0))
    + "</p>\n            <div>\n                "
    + ((stack1 = alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"cicd") : depth0)) != null ? lookupProperty(stack1,"icone") : stack1), depth0)) != null ? stack1 : "")
    + "\n                <p class=\"d-inline\">Etat: "
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"cicd") : depth0)) != null ? lookupProperty(stack1,"status") : stack1), depth0))
    + "</p>\n            </div>\n            <p><a href=\""
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"cicd") : depth0)) != null ? lookupProperty(stack1,"web_url") : stack1), depth0))
    + "\">Accéder à la pipeline</a></p>\n\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "            <p>Aucune pipeline trouvée.</p>\n";
},"16":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"row mt-4 text-center border-top\" style=\"display: block\">\n        <h4 class=\"text-decoration-underline mt-2\">Sujet</h4>\n\n        <button id=\"button_readme\" onclick=\"getFileContent("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":81,"column":59},"end":{"line":81,"column":65}}}) : helper)))
    + ", 'sujet.md', 'subject_content'); hide(this)\" type=\"button\"\n                class=\"btn btn-info\"\n                style=\"width: auto\">Afficher le sujet\n        </button>\n        <div id=\"subject_content\"></div>\n    </div>\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasReadme") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":87,"column":0},"end":{"line":97,"column":0}}})) != null ? stack1 : "");
},"19":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"row mt-4 text-center border-top\" style=\"display: block\">\n        <h4 class=\"text-decoration-underline mt-2\">Readme</h4>\n\n        <button id=\"button_readme\" onclick=\"getFileContent("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":91,"column":59},"end":{"line":91,"column":65}}}) : helper)))
    + ", 'README.md', 'readme_content'); hide(this)\" type=\"button\"\n                class=\"btn btn-info\"\n                style=\"width: auto\">Afficher mon README\n        </button>\n        <div id=\"readme_content\"></div>\n    </div>\n";
},"21":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"row mt-4 text-center border-top\" style=\"display: block\">\n        <h4 class=\"text-decoration-underline mt-2\">Rapport texte CI/CD</h4>\n\n        <button id=\"button_readme\" onclick=\"getFileContent("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":103,"column":59},"end":{"line":103,"column":65}}}) : helper)))
    + ", 'rapport.md', 'report_content'); hide(this)\" type=\"button\"\n                class=\"btn btn-info\"\n                style=\"width: auto\">Afficher le rapport de la dernière pipeline\n        </button>\n        <div id=\"report_content\"></div>\n    </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"col-md-12 text-center mt-4 mb-4\">\n    <h2>"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":2,"column":8},"end":{"line":2,"column":16}}}) : helper)))
    + "</h2>\n    <p class=\"mb-1\">Créé le "
    + alias4(((helper = (helper = lookupProperty(helpers,"created_at") || (depth0 != null ? lookupProperty(depth0,"created_at") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data,"loc":{"start":{"line":3,"column":28},"end":{"line":3,"column":42}}}) : helper)))
    + "</p>\n    <button onclick=\"location.href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"link") || (depth0 != null ? lookupProperty(depth0,"link") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link","hash":{},"data":data,"loc":{"start":{"line":4,"column":36},"end":{"line":4,"column":44}}}) : helper)))
    + "'\" class=\"btn btn-sm btn-primary\">Accéder à mon projet</button>\n\n</div>\n\n<div class=\"row border-top mb-2\">\n    <div class=\"col-md-6 mt-2 mb-2\">\n        <h4 class=\"text-decoration-underline\">Description</h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":11,"column":8},"end":{"line":15,"column":15}}})) != null ? stack1 : "")
    + "        <hr>\n\n        <h4>Etat de mon dépôt</h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"notPrivate") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":19,"column":8},"end":{"line":52,"column":15}}})) != null ? stack1 : "")
    + "\n    </div>\n    <div class=\"col-md-6 mt-2 mb-2\">\n        <h4 class=\"text-decoration-underline\">Cloner mon dépôt en SSH</h4>\n        <div class=\"d-inline mb-2\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ssh") || (depth0 != null ? lookupProperty(depth0,"ssh") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ssh","hash":{},"data":data,"loc":{"start":{"line":58,"column":12},"end":{"line":58,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n            <button type=\"button\" class=\"btn-clipboard\" title=\"Copier\" onclick=\"copySsh();\">Copier</button>\n        </div>\n        <hr>\n        <h4>Etat CI/CD (dernière pipeline)</h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,((stack1 = (depth0 != null ? lookupProperty(depth0,"cicd") : depth0)) != null ? lookupProperty(stack1,"created_at") : stack1),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.program(14, data, 0),"data":data,"loc":{"start":{"line":63,"column":8},"end":{"line":73,"column":15}}})) != null ? stack1 : "")
    + "    </div>\n</div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasSujet") : depth0),{"name":"if","hash":{},"fn":container.program(16, data, 0),"inverse":container.program(18, data, 0),"data":data,"loc":{"start":{"line":77,"column":0},"end":{"line":97,"column":7}}})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasReport") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":99,"column":0},"end":{"line":109,"column":7}}})) != null ? stack1 : "");
},"useData":true});
})();