(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['uploadFile'] = template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<hr>\n<div class=\"col-md-4\"></div>\n<div class=\"col-md-4 text-center mt-4 mb-4\">\n    <p>Le fichier que vous allez mettre en ligne doit être un fichier de code ou de texte (pas de binaire, exécutable, ou archive).</p>\n    <input type=\"file\" class=\"form-file-input\" id=\"customFile\" onchange=\"changeName(this.value)\">\n    <label for=\"customFile\">\n        <span class=\"form-file-text\" id=\"labelCustomFile\">Choisir un fichier...</span>\n        <span class=\"form-file-button\">Parcourir</span>\n        <button onclick=\"commitFile()\" class=\"btn btn-sm btn-primary\">Envoyer le fichier</button>\n    </label>\n</div>\n<div class=\"col-md-4\"></div>\n\n";
},"useData":true});
})();