(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['detailsTP'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <p>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":11,"column":15},"end":{"line":11,"column":30}}}) : helper)))
    + "</p>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "            <p>Aucune description trouvée.</p>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"row text-center mb-2 pb-2 pt-2 mt-2 border-top\" style=\"display: block\">\n        <h4 class=\"text-decoration-underline\">Sujet</h4>\n\n        <button id=\"button_readme\" onclick=\"getFileContent("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":29,"column":59},"end":{"line":29,"column":65}}}) : helper)))
    + ", 'sujet.md', 'subject_content'); hide(this)\" type=\"button\" class=\"btn btn-info\"\n                style=\"width: auto\">Afficher le sujet\n        </button>\n        <div id=\"subject_content\"></div>\n    </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"hasReadme") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":34,"column":0},"end":{"line":43,"column":0}}})) != null ? stack1 : "");
},"8":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div class=\"row text-center mb-2 pb-2 pt-2 mt-2 border-top\" style=\"display: block\">\n        <h4 class=\"text-decoration-underline\">Readme</h4>\n\n        <button id=\"button_readme\" onclick=\"getFileContent("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":38,"column":59},"end":{"line":38,"column":65}}}) : helper)))
    + ", 'README.md', 'readme_content'); hide(this)\" type=\"button\" class=\"btn btn-info\"\n                style=\"width: auto\">Afficher le README\n        </button>\n        <div id=\"readme_content\"></div>\n    </div>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "        <h2>Actions :</h2>\n        <button class=\"btn btn-primary\" onclick=\"hide($id('detailsTP'));acceptProject()\">Accepter ce projet</button>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"col-md-12 text-center mt-4 mb-4\">\n    <h2>"
    + alias4(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data,"loc":{"start":{"line":2,"column":8},"end":{"line":2,"column":16}}}) : helper)))
    + "</h2>\n    <p class=\"mb-1\">Créé le "
    + alias4(((helper = (helper = lookupProperty(helpers,"created_at") || (depth0 != null ? lookupProperty(depth0,"created_at") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"created_at","hash":{},"data":data,"loc":{"start":{"line":3,"column":28},"end":{"line":3,"column":42}}}) : helper)))
    + "</p>\n    <button onclick=\"location.href='"
    + alias4(((helper = (helper = lookupProperty(helpers,"link") || (depth0 != null ? lookupProperty(depth0,"link") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link","hash":{},"data":data,"loc":{"start":{"line":4,"column":36},"end":{"line":4,"column":44}}}) : helper)))
    + "'\" class=\"btn btn-sm btn-primary\">Accéder au projet</button>\n</div>\n\n<div class=\"row border-top mb-2\">\n    <div class=\"col-md-6 mt-2 mb-2\">\n        <h4 class=\"text-decoration-underline\">Description</h4>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":10,"column":8},"end":{"line":14,"column":15}}})) != null ? stack1 : "")
    + "    </div>\n    <div class=\"col-md-6 mt-2 mb-2\">\n        <h4 class=\"text-decoration-underline\">SSH To Repo</h4>\n        <div class=\"d-inline mb-2\">\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ssh") || (depth0 != null ? lookupProperty(depth0,"ssh") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ssh","hash":{},"data":data,"loc":{"start":{"line":19,"column":12},"end":{"line":19,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n            <button type=\"button\" class=\"btn-clipboard\" title=\"Copier\" onclick=\"copySsh();\">Copier</button>\n        </div>\n    </div>\n</div>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hasSujet") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":25,"column":0},"end":{"line":43,"column":7}}})) != null ? stack1 : "")
    + "\n<div class=\"col-md-12 text-center\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"accept") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":46,"column":4},"end":{"line":49,"column":11}}})) != null ? stack1 : "")
    + "</div>\n\n";
},"useData":true});
})();