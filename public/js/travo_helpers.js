// Fonctions utilitaires
function removeDuplicates( arr, prop ) {
    var obj = {};
    for ( var i = 0, len = arr.length; i < len; i++ ){
        if(!obj[arr[i][prop]]) obj[arr[i][prop]] = arr[i];
    }
    var newArr = [];
    for ( var key in obj ) newArr.push(obj[key]);
    return newArr;
}

function url_id() {
    const url = window.location.href;
    return (parseInt(getQueryParams(url, "project"), 10) || null);
}

function update_hist(stringified_item) {
    let ls_hist = JSON.parse(window.localStorage.getItem("hist_tp"));

    if (ls_hist) {
        ls_hist.push(JSON.parse(stringified_item));
        ls_hist = removeDuplicates(ls_hist, 'id_tp');
        localStorage.setItem('hist_tp', JSON.stringify(ls_hist));
    } else {
        ls_hist = '[' + stringified_item + ']';
        localStorage.setItem('hist_tp', ls_hist);
    }
}

function setForkedTPId(idTp, idFork) {
    console.log(idFork)
    let ls_hist = JSON.parse(window.localStorage.getItem("hist_tp"));
    for (let i = 0; i < ls_hist.length; i++) {
        if (ls_hist[i].id_tp === idTp) {
            ls_hist[i].id_fork = idFork;
            localStorage.setItem('hist_tp', JSON.stringify(ls_hist));
            return;
        }
    }
    return false;
}

function getHist(idTp) {
    let ls_hist = JSON.parse(window.localStorage.getItem("hist_tp"));
    for (let i = 0; i < ls_hist.length; i++) {
        if (ls_hist[i].id_tp === idTp) {
            return ls_hist[i];
        }
    }
    return false;
}

function copySsh() {
    const copyText = document.getElementById("sshUrl").textContent;
    navigator.clipboard.writeText(copyText).then(function () {
    }, function (err) {
        console.error("Async: Le texte n'a pas pu être copié ", err);
    });
}

function hide(element) {
    element.style.display = 'none';
}

function show(element) {
    element.style.display = 'block';
}

function b64DecodeUnicode(str) {
    return decodeURIComponent(atob(str).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

Date.prototype.addDays = function (days) {
    let date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
};

const $id = function (id) {
    return document.getElementById(id);
};

function getUserAuthorization() {
    window.location.href = `${url_oauth}?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=token&scope=api`;
}

function getQueryParams(url, param) {
    let href = url;
    let reg = new RegExp('[?&#]' + param + '=([^&#]*)', 'i');
    let queryString = reg.exec(href);
    return queryString ? queryString[1] : null;
}

function changeName() {
    let inputUpload = $id("customFile");
    if (inputUpload.files[0].name) {
        $id('labelCustomFile').innerHTML = inputUpload.files[0].name;
    }
}

const getUploadedFileContent = (inputFile = $id("customFile").files[0]) => {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve, reject) => {
        temporaryFileReader.onerror = () => {
            temporaryFileReader.abort();
            reject(new DOMException("Problem parsing input file."));
        };

        temporaryFileReader.onload = () => {
            resolve(temporaryFileReader.result);
        };
        temporaryFileReader.readAsText(inputFile);
    });
};
