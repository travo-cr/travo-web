// VARS TEST
const url_oauth = "https://gitlab-test.info.uqam.ca/oauth/authorize";
const client_id = "f9e3ce1d4ec29b8ec0544aed26d9320487f324e2648598373bcbf1ed81548566";
const redirect_uri = "https://info.pages.info.uqam.ca/travo-web/";
const url_api = "https://gitlab-test.info.uqam.ca/api/v4";

// const redirect_uri = "file:///home/pierre/WebstormProjects/travo/public/index.html";

// VARS PROD
// const url_oauth = "https://gitlab.info.uqam.ca/oauth/authorize";
// const client_id = "388f88baa296c324783a1ac2aec0589f8505678c94665a1bd5f7213810083bfe";
//const redirect_uri = "https://info.pages.info.uqam.ca/travo-web/";
// const url_api = "https://gitlab.info.uqam.ca/api/v4";


// CONST TEMPLATES COMPILES
const template_alert = Handlebars.templates.alerts;
const template_listTPsLS = Handlebars.templates.listTPsLS;
const template_infoFork = Handlebars.templates.infoFork;
const template_detailsTP = Handlebars.templates.detailsTP;
const template_detailsFork = Handlebars.templates.detailsFork;
const template_nav = Handlebars.templates.nav;
const template_uploadFile = Handlebars.templates.uploadFile;

async function checkUrl() {
    const url = window.location.href;

    const error_message = getQueryParams(url, "error");

    if (error_message === 'access_denied') {
        $id('error').innerHTML = template_alert({
            error: true,
            first_line: 'Vous avez refusé de donner accès à Travo.',
            second_line: 'Vous allez être redirigé automatiquement, merci d\'autoriser Travo à utiliser votre compte.'
        });
        setTimeout(function () {
            window.location.href = redirect_uri;
        }, 3000);
        return;
    }

    const project_id = url_id();

    const access_token = getQueryParams(url, "access_token");
    let access_token_ls = window.localStorage.getItem('access_token');
    let hist_tp = window.localStorage.getItem('hist_tp') || null;


    if (project_id === null && hist_tp !== null) {
        const hist_tp = JSON.parse(window.localStorage.getItem('hist_tp'));
        $id('TPsLs').innerHTML = template_listTPsLS({
            TPs: hist_tp
        });
    } else {
        // Si nouveaux arguments différents, suppression des anciens
        if ((access_token !== null && access_token_ls !== null && access_token !== access_token_ls) || (access_token !== null && access_token_ls === null)) {
            window.localStorage.setItem('access_token', `${access_token}`);
            access_token_ls = window.localStorage.getItem('access_token');
            window.location.href = window.location.href.split("#")[0];

        } else if (access_token !== null && access_token === access_token_ls) {
            window.location.href = window.location.href.split("#")[0];
        }


        // Si aucun argument donné, et rien dans le local storage, afficher message d'erreur et quitter
        if (access_token_ls === null && project_id === null) {
            $id('error').innerHTML = template_alert({
                error: true,
                first_line: 'Aucun argument dans l\'url, et aucune valeur dans le local storage.',
                second_line: 'Utilisez-vous bien une URL fournie par un professeur ?',
                clear_cache: true
            });
            return;
        }

        // Si argument NaN ou vide et rien dans le localstorage
        if (project_id === null && project_id === null) {
            $id('error').innerHTML = template_alert({
                error: true,
                first_line: 'L\'argument du projet n\'est pas un nombre. Merci de vérifier avec votre professeur que l\'URL est bien la bonne.'
            });
            return;
        }
        if (access_token_ls !== null && project_id !== null) {
            showUserMenu();

            if (await isAlreadyForked()) {
                await showForkedProjectInfos();
            } else {
                getProjectInfos();
            }
        } else {
            show($id("buttonLogin"));
        }
    }
}

async function showProjectInfos(res) {
    let new_hist = {
        id_tp: res.id,
        id_prof: res.creator_id,
        name: res.name,
    };
    update_hist(JSON.stringify(new_hist));


    let subject_exist = await fileExist(res.id, 'sujet.md');
    let readme_exist = await fileExist(res.id, 'README.md');
    $id('detailsTP').innerHTML = template_detailsTP({
        id: res.id,
        name: res.name,
        description: res.description,
        created_at: new Date(res.created_at).toLocaleString(),
        link: res.web_url,
        hasSujet: subject_exist,
        hasReadme: readme_exist,
        ssh: '<pre style="display: inline"><code id="sshUrl">' + res.ssh_url_to_repo + '</code></pre>',
        accept: true,
    });
}

async function showForkedProjectInfos() {
    const project_id = url_id();
    const TPInfo = getHist(project_id);
    let prof_id = parseInt(TPInfo.id_prof, 10);
    let forked_project_id = parseInt(TPInfo.id_fork, 10);
    const project_infos = await getForkedProjectInfos();
    let last_pipeline;

    if (project_infos) {
        const members_infos = await getMembersInfos();
        const pipeline_infos = await getPipelineInfos();
        let subject_exist = await fileExist(forked_project_id, 'sujet.md');
        let readme_exist = await fileExist(forked_project_id, 'README.md');
        if ((await pipeline_infos).length !== 0) {
            last_pipeline = pipeline_infos[0];
        }

        let isNotPrivate;
        project_infos.visibility !== 'private' ? isNotPrivate = true : isNotPrivate = false;

        if (last_pipeline) {
            switch (last_pipeline.status) {
                case "failed":
                case "warning":
                case "canceled":
                case "skipped":
                default:
                    html_icon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-exclamation-triangle text-warning" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M7.938 2.016a.146.146 0 0 0-.054.057L1.027 13.74a.176.176 0 0 0-.002.183c.016.03.037.05.054.06.015.01.034.017.066.017h13.713a.12.12 0 0 0 .066-.017.163.163 0 0 0 .055-.06.176.176 0 0 0-.003-.183L8.12 2.073a.146.146 0 0 0-.054-.057A.13.13 0 0 0 8.002 2a.13.13 0 0 0-.064.016zm1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995z"/></svg>';
                    break;
                case "pending":
                case "running":
                case "scheduled":
                case "created":
                case "manual":
                    html_icon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-clock text-info" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z"/><path fill-rule="evenodd" d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z"/></svg>';
                    break;
                case "success":
                    html_icon = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check2 text-success" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"/></svg>';
                    break;
            }

            let has_report = await fileExist(project_infos.id, 'rapport.md');

            $id('detailsFork').innerHTML = template_detailsFork({
                id: project_infos.id,
                name: project_infos.name,
                description: project_infos.description,
                notPrivate: isNotPrivate,
                hasReadme: readme_exist,
                hasSujet: subject_exist,
                created_at: new Date(project_infos.created_at).toLocaleString(),
                link: project_infos.web_url,
                ssh: '<pre style="display: inline"><code id="sshUrl">' + project_infos.ssh_url_to_repo + '</code></pre>',
                cicd: {
                    created_at: new Date(last_pipeline.created_at).toLocaleString(),
                    id: last_pipeline.id,
                    status: last_pipeline.status,
                    icone: html_icon,
                    web_url: last_pipeline.web_url
                },
                hasReport: has_report,
                profAdded: members_infos.some(item => item.id === prof_id),
            });

        } else {
            $id('detailsFork').innerHTML = template_detailsFork({
                id: project_infos.id,
                name: project_infos.name,
                description: project_infos.description,
                notPrivate: isNotPrivate,
                hasReadme: readme_exist,
                hasSujet: subject_exist,
                created_at: new Date(project_infos.created_at).toLocaleString(),
                link: project_infos.web_url,
                ssh: '<pre style="display: inline"><code id="sshUrl">' + project_infos.ssh_url_to_repo + '</code></pre>',
                profAdded: members_infos.some(item => item.id === prof_id),
            });
        }

        $id('uploadFile').innerHTML = template_uploadFile();
    }
}

function acceptProject() {
    $id('infoFork').innerHTML = template_infoFork({
        title: 'Clonage en cours...'
    });
    forkProject();
}

async function isAlreadyForked() {
    let project_id = url_id();
    if (window.localStorage.getItem("hist_tp")) {
        const TPInfo = getHist(project_id);
        if (TPInfo.id_fork) {
            let forked_project_id_ls = parseInt(TPInfo.id_fork, 10);
            if (Number.isInteger(forked_project_id_ls)) {
                return true;
            }
        } else if (TPInfo === false) {
            let parentTP = await getParentInfos();
            console.log(parentTP)
            let new_hist = {
                id_tp: parentTP.id,
                id_prof: parentTP.creator_id,
                name: parentTP.name,
            };
            update_hist(JSON.stringify(new_hist));

        }

    }


    const sha_parent = await getShaParentProject(project_id);
    let student_projects = await getStudentProjects();
    if (student_projects.length === 0) {
        return false;
    }

    for (const e of student_projects) {
        if (await checkSha(e, sha_parent)) {
            setForkedTPId(project_id, e);
            return true;
        }

    }

    return false;
}
