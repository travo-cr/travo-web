function getProjectInfos() {
    let project_id = url_id();
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${project_id}`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.error === "invalid_token") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Votre Token est invalide. Avez-vous retiré l\'accès à l\'application de votre profil Gitlab ?',
                    second_line: 'Si oui, merci de vous reconnecter après avoir vidé votre Local Storage.',
                    clear_cache: true
                });
                return;
            } else if (res.message === "404 Project Not Found") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Votre professeur vous a-t-il bien communiqué la bonne URL ?'
                });
                return;
            } else if (res.message === "401 Unauthorized") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Avez-vous bien été ajouté au dépôt Gitlab du TP ?'
                });
                return;
            } else {
                showProjectInfos(res);
                return;
            }
        })
        .catch(error => console.log('error', error));
}

async function getParentInfos() {
    let project_id = url_id();
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let response = await fetch(`${url_api}/projects/${project_id}`, requestOptions);
    let json_res = await response.json();
    return json_res;
}

async function getForkedProjectInfos() {
    let project_id = url_id();
    const TPInfo = getHist(project_id);
    project_id = parseInt(TPInfo.id_fork, 10);
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    const result = await fetch(`${url_api}/projects/${project_id}?statistics=true`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.error === "invalid_token") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Votre Token est invalide. Avez-vous retiré l\'accès à l\'application de votre profil Gitlab ?',
                    second_line: 'Si oui, merci de vous reconnecter après avoir vidé votre Local Storage.',
                    clear_cache: true
                });
                return;
            } else if (res.message === "404 Project Not Found") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Votre projet est introuvable.',
                    second_line: 'Merci de vider votre cache et de ré-essayer.',
                    clear_cache: true
                });
                return;
            } else if (res.message === "401 Unauthorized") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Avez-vous bien été ajouté au dépôt Gitlab du TP ?'
                });
                return;
            } else {
                return res;
            }
        })
        .catch(error => console.log('error', error));
    return result;
}


async function fileExist(project_id = parseInt(getHist(url_id()).id_fork, 10), file_name = document.getElementById("customFile").files[0].name) {
    let access_token_ls = window.localStorage.getItem('access_token');
    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);
    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let result = await fetch(`${url_api}/projects/${project_id}/repository/files/${file_name}?ref=master`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.error === "invalid_token") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Votre Token est invalide. Avez-vous retiré l\'accès à l\'application de votre profil Gitlab ?',
                    second_line: 'Si oui, merci de vous reconnecter après avoir vidé votre Local Storage.',
                    clear_cache: true
                });
                return false;
            } else if (res.message === "404 Project Not Found") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Votre professeur vous a-t-il bien communiqué la bonne URL ?'
                });
                return false;
            } else if (res.message === "401 Unauthorized") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Avez-vous bien été ajouté au dépôt Gitlab du TP ?'
                });
                return false;
            } else if (res.message === "404 File Not Found") {
                return false;
            } else {
                return true;
            }
        })
        .catch(error => console.log('error', error));

    return result;
}

async function getMembersInfos(id_project = parseInt(getHist(url_id()).id_fork, 10)) {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    const result = await fetch(`${url_api}/projects/${id_project}/members/all`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.error === "invalid_token") {
                if ($id('error')) {
                    $id('error').innerHTML = template_alert({
                        error: true,
                        first_line: 'Votre Token est invalide. Avez-vous retiré l\'accès à l\'application de votre profil Gitlab ?',
                        second_line: 'Si oui, merci de vous reconnecter après avoir vidé votre Local Storage.',
                        clear_cache: true
                    });
                }

                return false;
            } else if (res.message === "404 Project Not Found") {
                if ($id('error')) {
                    $id('error').innerHTML = template_alert({
                        error: true,
                        first_line: 'Le projet est introuvable.',
                        second_line: 'Votre professeur vous a-t-il bien communiqué la bonne URL ?'
                    });
                }

                return false;
            } else if (res.message === "401 Unauthorized") {
                if ($id('error')) {
                    $id('error').innerHTML = template_alert({
                        error: true,
                        first_line: 'Le projet est introuvable.',
                        second_line: 'Avez-vous bien été ajouté au dépôt Gitlab du TP ?'
                    });
                }

                return false;
            } else {
                return res;
            }
        })
        .catch(error => console.log('error', error));
    return result;
}

async function getPipelineInfos(id_project = parseInt(getHist(url_id()).id_fork, 10)) {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    const result = await fetch(`${url_api}/projects/${id_project}/pipelines?order_by=updated_at`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.error === "invalid_token") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Votre Token est invalide. Avez-vous retiré l\'accès à l\'application de votre profil Gitlab ?',
                    second_line: 'Si oui, merci de vous reconnecter après avoir vidé votre Local Storage.',
                    clear_cache: true
                });
                return;
            } else if (res.message === "404 Project Not Found") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Votre professeur vous a-t-il bien communiqué la bonne URL ?'
                });
                return;
            } else if (res.message === "401 Unauthorized") {
                $id('error').innerHTML = template_alert({
                    error: true,
                    first_line: 'Le projet est introuvable.',
                    second_line: 'Avez-vous bien été ajouté au dépôt Gitlab du TP ?'
                });
                return;
            } else {
                return res;
            }
        })
        .catch(error => console.log('error', error));
    return result;
}

async function commitFile(id_project = parseInt(getHist(url_id()).id_fork, 10)) {

    let file_content;

    file_content = await getUploadedFileContent();

    if (file_content) {
        const file_exist = await fileExist();
        const file_name = document.getElementById("customFile").files[0].name;
        let raw;
        let access_token_ls = window.localStorage.getItem('access_token');
        let myHeaders = new Headers();
        myHeaders.append("Authorization", `Bearer ${access_token_ls}`);
        myHeaders.append("Content-Type", "application/json");

        if (file_exist) {
            raw = JSON.stringify({
                "branch": "master", "actions": [{
                    "action": "update",
                    "file_path": file_name,
                    "content": file_content,
                }], "commit_message": "Remise d'un fichier avec Travo Web."
            });
        } else {
            raw = JSON.stringify({
                "branch": "master", "actions": [{
                    "action": "create",
                    "file_path": file_name,
                    "content": file_content,
                }], "commit_message": "Remise d'un fichier avec Travo Web."
            });
        }

        let requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch(`${url_api}/projects/${id_project}/repository/commits`, requestOptions)
            .then(response => response.text())
            .then(result => {
                let res = JSON.parse(result);
                $id('error').innerHTML = template_alert({
                    error: false,
                    first_line: 'Le fichier a bien été mis en ligne sur votre dépôt Gitlab.',
                });
            })
            .catch(error => console.log('error', error));
    } else {
        $id('error').innerHTML = template_alert({
            error: true,
            first_line: 'Erreur lors de la lecture du fichier.',
        });
    }
}

function showUserMenu() {
    let access_token_ls = window.localStorage.getItem('access_token');
    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/user`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            $id('nav').innerHTML = template_nav({
                connected: {
                    name: res.name,
                    image: res.avatar_url
                }
            });
        })
        .catch(error => console.log('error', error));


}

function getFileContent(id_project = parseInt(getHist(url_id()).id_fork, 10), file_name = 'README.md', div_name = 'readme_content') {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${id_project}/repository/files/${file_name}?ref=master`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            b64ToHTML(b64DecodeUnicode(res.content), div_name);

        })
        .catch(error => console.log('error', error));
}

function b64ToHTML(res, div_name) {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);
    myHeaders.append("Content-Type", "application/json");
    let raw = JSON.stringify({"text": res, "gfm": true});

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
    };

    fetch(`${url_api}/markdown`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let htmlResult = JSON.parse(result);
            $id(div_name).innerHTML = htmlResult.html;
        })
        .catch(error => console.log('error', error));
}

function forkProject() {
    let project_id = url_id();
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${project_id}/fork`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            if (res.message) {
                console.log('Un fork ou un projet avec le même nom existe déjà dans votre espace local.');
                return;
            } else {
                $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
                    title: 'Projet clôné, passage du dépôt en privé.',
                });

                setForkedTPId(project_id, res.id);
                if (res.visibility !== 'private') {
                    makePrivate(res.id);
                } else {
                    addProfessor(res.id);
                }
		addForkRelationship(res.id, project_id);
            }
        })
        .catch(error => console.log('error', error));
}

function makePrivate(id_project = parseInt(getHist(url_id()).id_fork, 10)) {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${id_project}?visibility=private`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
                title: 'Projet passé en privé avec succès.',
            });

            addProfessor();
        })
        .catch(error => console.log('error', error));
}

function addProfessor(id_project = parseInt(getHist(url_id()).id_fork, 10)) {
    $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
        title: 'Ajout du professeur en cours...',
    });

    let access_token_ls = window.localStorage.getItem('access_token');
    let prof_id_ls = parseInt(getHist(url_id()).id_prof, 10);

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${id_project}/members?user_id=${prof_id_ls}&access_level=40`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
                title: 'Professeur ajouté avec succès.',
            });
            $id('infoFork').innerHTML = '';

            getForkedProjectInfos();

            document.location.reload();
        })
        .catch(error => console.log('error', error));
}

function addForkRelationship(id_project, forked_from_id) {
    $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
        title: 'Ajout du lien de fork en cours...',
    });

    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'POST',
        headers: myHeaders,
        redirect: 'follow'
    };

    fetch(`${url_api}/projects/${id_project}/fork/${forked_from_id}`, requestOptions)
        .then(response => response.text())
        .then(result => {
            let res = JSON.parse(result);
            $id('infoFork').innerHTML = $id('infoFork').innerHTML + template_infoFork({
                title: 'Lien de fork ajouté avec succès.',
            });
            $id('infoFork').innerHTML = '';

            getForkedProjectInfos();

            document.location.reload();
        })
        .catch(error => console.log('error', error));

}

async function checkSha(project_id, sha) {
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let response = await fetch(`${url_api}/projects/${project_id}/repository/commits/${sha}`, requestOptions);
    let json_res = await response.json();
    if (json_res.message === "404 Commit Not Found") {
        return false;
    } else {
        return true;
    }
}

async function getStudentProjects() {
    let access_token_ls = window.localStorage.getItem('access_token');
    let project_id = url_id();

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);
    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };
    let response = await fetch(`${url_api}/projects?owned=true&id_after=${project_id}&per_page=100`, requestOptions);
    let json_res = await response.json();
    let permittedValues = await json_res.map(value => value.id);

    return permittedValues;

}

// La logique pour retrouver le premier commit est la suivante :
// On prend la date de création du dépôt, on y ajoute deux semaines, et on prend le tout premier commit.
// Cela permet de réduire le temps de cette requête, et de surtout permettre de récupérer le premier commit même s'il y a plus de 100 commits sur le dépôt parent, sans avoir à gérer la pagination
async function getShaParentProject(project_id) {
    let project_id_sha = parseInt(project_id, 10);
    let access_token_ls = window.localStorage.getItem('access_token');
    const before_date = await getCreationDate(project_id_sha);

    let creation_date = before_date.created_at;
    if (window.localStorage.getItem('hist_tp') === null) {
        let new_hist = {
            id_tp: before_date.id,
            id_prof: before_date.creator_id,
            name: before_date.name,
        };
        update_hist(JSON.stringify(new_hist));
    }

    creation_date = await new Date(creation_date).addDays(14).toISOString();
    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let response = await fetch(`${url_api}/projects/${project_id_sha}/repository/commits?per_page=100&first_parent=true&until=${creation_date}`, requestOptions);
    let json_res = await response.json();
    let data = await json_res[json_res.length - 1];
    return data.id;
}

async function getCreationDate(project_id) {
    let project_id_date = parseInt(project_id, 10);
    let access_token_ls = window.localStorage.getItem('access_token');

    let myHeaders = new Headers();
    myHeaders.append("Authorization", `Bearer ${access_token_ls}`);

    let requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
    };

    let response = await fetch(`${url_api}/projects/${project_id_date}`, requestOptions);
    return await response.json();
}
